import math
from typing import List, Optional

from inkmeasure.util import integrate, gaussian


KeyFrame = int


class ErrorAreas:
  def __init__(self):
    self.tp: Optional[float] = None
    self.tn: Optional[float] = None
    self.fp: Optional[float] = None
    self.fn: Optional[float] = None

  def __radd__(self, other):
    if isinstance(other, float) or isinstance(other, int):
      a = ErrorAreas()
      if self.tp:
        a.tp = self.tp + other
      if self.tn:
        a.tn = self.tn + other
      if self.fp:
        a.fp = self.tp + other
      if self.fn:
        a.fn = self.fn + other
      return a
    else:
      raise ArithmeticError('Unsupported type. {}'.format(other))

  def __add__(self, other):
    if isinstance(other, ErrorAreas):
      a = ErrorAreas()
      if self.tp and other.tp:
        a.tp = self.tp + other.tp
      if self.tn and other.tn:
        a.tn = self.tn + other.tn
      if self.fp and other.fp:
        a.fp = self.tp + other.fp
      if self.fn and other.fn:
        a.fn = self.fn + other.fn
      return a
    else:
      raise ArithmeticError('Unsupported type. {}'.format(other))


class EvaluationResults:
  def __init__(self):
    self.precision: Optional[float] = None
    self.recall: Optional[float] = None
    self.accuracy: Optional[float] = None
    self.f1: Optional[float] = None
    self.mcc: Optional[float] = None

  def __radd__(self, other):
    if isinstance(other, float) or isinstance(other, int):
      r = EvaluationResults()
      if self.precision:
        r.precision = self.precision + other
      if self.recall:
        r.recall = self.recall + other
      if self.accuracy:
        r.accuracy = self.accuracy + other
      if self.f1:
        r.f1 = self.f1 + other
      if self.mcc:
        r.mcc = self.mcc + other
      return r
    else:
      raise ArithmeticError('Unsupported type. {}'.format(other))

  def __add__(self, other):
    if isinstance(other, EvaluationResults):
      r = EvaluationResults()
      if self.precision and other.precision:
        r.precision = self.precision + other.precision
      if self.recall and other.recall:
        r.recall = self.recall + other.recall
      if self.accuracy and other.accuracy:
        r.accuracy = self.accuracy + other.accuracy
      if self.f1 and other.f1:
        r.f1 = self.f1 + other.f1
      if self.mcc and other.mcc:
        r.mcc = self.mcc + other.mcc
      return r
    else:
      raise ArithmeticError('Unsupported type. {}'.format(other))

  def __rmul__(self, other):
    if isinstance(other, float) or isinstance(other, int):
      r = EvaluationResults()
      if self.precision:
        r.precision = self.precision * other
      if self.recall:
        r.recall = self.recall * other
      if self.accuracy:
        r.accuracy = self.accuracy * other
      if self.f1:
        r.f1 = self.f1 * other
      if self.mcc:
        r.mcc = self.mcc * other
      return r
    else:
      raise ArithmeticError('Unsupported type. {}'.format(other))


def calc_error_areas(gt: List[KeyFrame], seg: List[KeyFrame], frame_count: int, sigma: float) -> ErrorAreas:
  assert all(0 <= x < frame_count for x in gt)
  assert all(0 <= x < frame_count for x in seg)
  assert sigma >= 0

  # Define relevant functions.
  # Note: Using the self-implemented custom "gaussian" function over scipy.stats.norm.pdf or _pdf is significantly
  # faster for unknown reasons.
  f_s = lambda x: sum(gaussian(x, kf, sigma) for kf in seg)
  f_gt = lambda x: -sum(gaussian(x, kf, sigma) for kf in gt)
  e_c = lambda x: f_s(x) + f_gt(x)
  e_fp = lambda x: max(e_c(x), 0)
  e_fn = lambda x: min(e_c(x), 0)

  a = ErrorAreas()

  # Integral borders.
  int_min = 0.0 - 3 * sigma
  int_max = float(frame_count) + 3 * sigma

  # Calculate integrals for e_fp and e_fn to get fp and fn. Calculate tp and tn then.
  a.fp = integrate(e_fp, int_min, int_max)
  a.fn = abs(integrate(e_fn, int_min, int_max))
  a.tp = len(seg) - a.fp
  a.tn = frame_count - a.tp - a.fp - a.fn

  assert 0 <= a.fp
  assert 0 <= a.fn
  assert 0 <= a.tp
  assert 0 <= a.tn

  return a


def evaluate(a: ErrorAreas) -> EvaluationResults:
  r = EvaluationResults()
  r.precision = a.tp / (a.tp + a.fp)
  r.recall = a.tp / (a.tp + a.fn)
  r.accuracy = (a.tp + a.tn) / (a.tp + a.tn + a.fp + a.fn)
  r.f1 = 2 * ((r.precision * r.recall) / (r.precision + r.recall))
  r.mcc = ((a.tp * a.tn) - (a.fp * a.fn)) / (math.sqrt((a.tp + a.fp) * (a.tp + a.fn) * (a.tn + a.fp) * (a.tn + a.fn)))
  return  r
