import math
import numpy
import scipy.integrate


def integrate(fn, lower: float, upper: float, step: float = 0.05) -> float:
  ys = []
  for x in numpy.arange(lower, upper, step):
    y=fn(x)
    ys.append(y)
  integral = scipy.integrate.trapz(ys, dx=step)
  return integral


def gaussian(x: float, mu: float, sigma: float) -> float:
  sigma_squared = sigma**2
  x_minus_mu = x - mu
  factor = 1.0 / math.sqrt(2.0 * math.pi * sigma_squared)
  exp_arg = -(x_minus_mu**2) / (2.0 * sigma_squared)
  return factor * math.exp(exp_arg)
