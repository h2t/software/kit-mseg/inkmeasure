import pytest

from inkmeasure import binary


gt = [0, 24, 49]
seg_a = [0, 24, 49]
seg_b = [1, 25, 50]
seg_c = [1, 23, 25, 50]
seg_d = [1, 23, 25, 60]
seg_e = [2, 22, 26, 61]
seg_f = [6, 18, 30, 65]
seg_g = [11, 13, 35, 70]
frame_count = 71
sigma = 1.67
epsilon = 0.0049


def test_seg_a():
  a = binary.calc_error_areas(gt, seg_a, frame_count=frame_count, sigma=sigma)
  r = binary.evaluate(a)
  assert r.f1 == pytest.approx(1.00, abs=epsilon)


def test_seg_b():
  a = binary.calc_error_areas(gt, seg_b, frame_count=frame_count, sigma=sigma)
  r = binary.evaluate(a)
  assert r.f1 == pytest.approx(0.76, abs=epsilon)


def test_seg_c():
  a = binary.calc_error_areas(gt, seg_c, frame_count=frame_count, sigma=sigma)
  r = binary.evaluate(a)
  assert r.f1 == pytest.approx(0.72, abs=epsilon)


def test_seg_d():
  a = binary.calc_error_areas(gt, seg_d, frame_count=frame_count, sigma=sigma)
  r = binary.evaluate(a)
  assert r.f1 == pytest.approx(0.50, abs=epsilon)


def test_seg_e():
  a = binary.calc_error_areas(gt, seg_e, frame_count=frame_count, sigma=sigma)
  r = binary.evaluate(a)
  assert r.f1 == pytest.approx(0.44, abs=epsilon)


def test_seg_f():
  a = binary.calc_error_areas(gt, seg_f, frame_count=frame_count, sigma=sigma)
  r = binary.evaluate(a)
  assert r.f1 == pytest.approx(0.06, abs=epsilon)


def test_seg_g():
  a = binary.calc_error_areas(gt, seg_g, frame_count=frame_count, sigma=sigma)
  r = binary.evaluate(a)
  assert r.f1 == pytest.approx(0.00, abs=epsilon)
